---------- Back end APIs Development ----

I. prerequisites

1. Spring STS 4 + version
2. Mysql 8.023 server and workbench
3. Apache Maven 3.6.3
4. git version 2.25.0
5. Bit bucket registered

II. setup the apis

1. open cmd and run the clone command after entering in E-ID folder

git clone https://Deep231992@bitbucket.org/Deep231992/fun_zone.git

2. After cloning the project, open the same in Spring STS using maven existing project



3. Make sure the database password and username is same as mentioned in application.properties file.

4. create a database named gameapplication in mysql workbench as mentioned in property file: 

5. run the file gamewebapplication.java via:

run as spring boot app

6.  when application starts running - open the swagger url . this URL is going to provide you the API instructions and all the info related to Rest apis of this application. 

http://localhost:8080/swagger-ui/




III.    Different tools and Apis being used in this app

1. Swagger - It's a very powerfull tool which  is going to give a document structure for this Rest api application which will have all the API related information

2. embedded tomcat - This Rest Api application is using embedded tomcat which comes with spring boot application automatically. No external server required to run this web app. In-case of 
using this app in another server and external server. some configuration needs to be changed.

3. Spring Boot and JPA - This is the rapid api development framework build on top of spring framework. creates the tables in db automatically.











