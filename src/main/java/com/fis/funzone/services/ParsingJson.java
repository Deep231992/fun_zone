package com.fis.funzone.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fis.funzone.model.Event;
import com.fis.funzone.model.GameGroup;
import com.fis.funzone.model.GameUser;
import com.fis.funzone.model.Games;
import com.fis.funzone.model.GroupGameMapping;
import com.fis.funzone.model.UserGroupMapping;
import com.fis.funzone.repository.EventRepository;
import com.fis.funzone.repository.GameGroupRepository;
import com.fis.funzone.repository.GameUserRepository;
import com.fis.funzone.repository.GamesRepository;

import springfox.documentation.spring.web.json.Json;

@Service
public class ParsingJson {

	@Autowired
	GameUserRepository userRepository;
	
	@Autowired
	GamesRepository gamesRepository;
	
	@Autowired
	GameGroupRepository gameGroupRepository;
	
	 @Autowired
	    private JavaMailSender javaMailSender;
	 
	 @Value("${funzoneurl}")
	 private String url;
	
	
	
	
public Event parseJsonString(String json) {
	Event event=new Event();
	try {
		JSONObject obj = new JSONObject(json);
		
		event.setEventName(obj.getString("eventName"));
		event.setEvent_Duration(obj.getInt("Event_Duration"));
		event.setEvent_status("N");
		Random random = new Random();    
		long n = (long) (100000000000000L + random.nextFloat() * 900000000000000L);
		event.setEventId(n);
		event.setCreatedBy(obj.getString("createdBy"));
		GameGroup gameGroup=new GameGroup();
//		gameGroup.setEvent(event);
		gameGroup.setGroupName(obj.getString("groupName"));
		
		Set<UserGroupMapping> setHgm=new HashSet<UserGroupMapping>();
		Set<GroupGameMapping> setggm=new HashSet<GroupGameMapping>();
		
		for(int i=0; i<obj.getJSONArray("userList").length(); i++) {
			UserGroupMapping  ugm=new UserGroupMapping();
		GameUser user=	userRepository.findByUserId(obj.getJSONArray("userList").getString(i));
		ugm.setGameUser(user);
		ugm.setGameGroup(gameGroup);
		setHgm.add(ugm);
		}
		
		for(int i=0; i<obj.getJSONArray("gamesList").length(); i++) {
			GroupGameMapping  ggm=new GroupGameMapping();
		Games game=	gamesRepository.findByGameName(obj.getJSONArray("gamesList").getString(i));
		ggm.setGames(game);
		ggm.setGameGroup(gameGroup);
		setggm.add(ggm);
		}
		gameGroup.setUserGroupMapping(setHgm);
		gameGroup.setGroupGameMapping(setggm);
		
		gameGroupRepository.save(gameGroup);
		event.setGameGroup(gameGroup);
		
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return event;
}	

public String fetchEventData(Event event) throws JsonProcessingException {
	Set<GroupGameMapping> ggp=event.getGameGroup().getGroupGameMapping();
	Set<UserGroupMapping> Ugp=event.getGameGroup().getUserGroupMapping();
	Map<String,List<GameUser>> map=new LinkedHashMap<String,List<GameUser>>();
	
	for(GroupGameMapping groupGameMapping : ggp){
		   System.out.println("games-->"+groupGameMapping.getGames().getGameName());
		   List<GameUser> users=new ArrayList<>();
		   for(UserGroupMapping userGroupMapping : Ugp){
//			   System.out.println("gameUsers--->"+userGroupMapping.getGameUser().getUserId());
			   users.add(userGroupMapping.getGameUser());
		   }
		   map.put(groupGameMapping.getGames().getGameName(), users);
		}
	System.out.println("map--->"+map);
	ObjectMapper mapper = new ObjectMapper();
	mapper.findAndRegisterModules();
	return mapper.writeValueAsString(map);
}

public void sendEmail(Event event) throws MessagingException {
	
    
    Set<UserGroupMapping> ugm=event.getGameGroup().getUserGroupMapping();
    

    
    
    
    for(UserGroupMapping userGroup : ugm) {
   	 MimeMessage message = javaMailSender.createMimeMessage();
   	String content = "Dear [[name]],<br>"
            + "Please click the link below to enter in to the event :<br>"
            + "<h3><a href=\"[[URL]]\" target=\"_self\">Event</a></h3>"
            + "Thank you,<br>"
            + "FIS GLOBAL";
   	content = content.replace("[[URL]]", url+"?eventId="+event.getEventId());
	    MimeMessageHelper helper = new MimeMessageHelper(message);

    	helper.setTo(userGroup.getGameUser().getEmailId());
    	content = content.replace("[[name]]", userGroup.getGameUser().getUserName());
    	helper.setText(content,true);
    	helper.setSubject("Invitation from Funzone event");
    	javaMailSender.send(message);	
    }
  

    

}


	
}
