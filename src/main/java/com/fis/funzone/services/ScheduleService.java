package com.fis.funzone.services;

import org.springframework.beans.factory.annotation.Autowired;

import com.fis.funzone.repository.EventRepository;

public class ScheduleService extends Thread {

//@Autowired
//EventRepository eventRepository;
	
	
public int timeInterval;
public long eventId;
public EventRepository eventRepository;
	
public ScheduleService(int timeInterval,long eventId,EventRepository eventRepository) {
 this.timeInterval=timeInterval;
 this.eventId=eventId;
 this.eventRepository=eventRepository;
	
}



	@Override
	public void run() {
		try {
			Thread.sleep(this.timeInterval * 60000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		eventRepository.updateEvent("N",this.eventId);
		
	}

}
