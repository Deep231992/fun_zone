package com.fis.funzone.responseModel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fis.funzone.model.GameUser;

public class LoginResponse {
	
	String msg;
	String userName;
	String empId;
	String roleName;
	Long roleId;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}


	



	int returnCode;
	String httpStatus;
	
	private LocalDateTime LastLoginTime;
	
	
	public LoginResponse(String msg, String userName, String empId, String roleName, Long roleId, int returnCode,
			String httpStatus, LocalDateTime lastLoginTime) {
		super();
		this.msg = msg;
		this.userName = userName;
		this.empId = empId;
		this.roleName = roleName;
		this.roleId = roleId;
		this.returnCode = returnCode;
		this.httpStatus = httpStatus;
		LastLoginTime = lastLoginTime;
	}
	
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	
	
	public LocalDateTime getLastLoginTime() {
		return LastLoginTime;
	}
	public void setLastLoginTime(LocalDateTime lastLoginTime) {
		LastLoginTime = lastLoginTime;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public int getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}
	public String getHttpStatus() {
		return httpStatus;
	}
	public void setHttpStatus(String httpStatus) {
		this.httpStatus = httpStatus;
	}

}
