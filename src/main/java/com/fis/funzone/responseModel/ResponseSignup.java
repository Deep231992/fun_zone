package com.fis.funzone.responseModel;

public class ResponseSignup {

private String message;
private String httpStatus;
private int returnCode;
public ResponseSignup(String message, String httpStatus, int returnCode) {
	super();
	this.message = message;
	this.httpStatus = httpStatus;
	this.returnCode = returnCode;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public String getHttpStatus() {
	return httpStatus;
}
public void setHttpStatus(String httpStatus) {
	this.httpStatus = httpStatus;
}
public int getReturnCode() {
	return returnCode;
}
public void setReturnCode(int returnCode) {
	this.returnCode = returnCode;
}
	
	
}
