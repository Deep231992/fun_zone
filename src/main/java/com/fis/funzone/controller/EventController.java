package com.fis.funzone.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fis.funzone.model.Event;
import com.fis.funzone.repository.EventRepository;
import com.fis.funzone.services.ParsingJson;
import com.fis.funzone.services.ScheduleService;

@RestController
@RequestMapping("/event")
@CrossOrigin(origins = "http://localhost:8080")
public class EventController {
	
	@Autowired
	ParsingJson parsingjson;
	
	@Autowired
	EventRepository eventRepository;
	
	
	@PostMapping(value = "/createEvent",produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE
			)
	public long createEvent(@RequestBody String payload) throws MessagingException {
		System.out.println("payload-->"+payload);
		Event event=parsingjson.parseJsonString(payload);
		eventRepository.save(event);
		parsingjson.sendEmail(event);
		return event.getEventId();
		

	}
	
	@GetMapping("/getAllEvents")
	public List<Event> fetchEvent() {
		List<Event> events=eventRepository.findAll();
		
		return events;
	}
	
	@GetMapping(value = "/getEventData/{eventId}",produces = MediaType.APPLICATION_JSON_VALUE)
	public String fetchEventData(@PathVariable long eventId) throws JsonProcessingException {
	
		Event event=eventRepository.findByEventId(eventId);
		return parsingjson.fetchEventData(event);
		
		
		
	}
	
	@GetMapping("/startEvent/{eventId}")
	public boolean startEvent(@PathVariable Long eventId) {
		
		eventRepository.updateEvent("S",eventId);
		
		new ScheduleService(eventRepository.findByEventId(eventId).getEvent_Duration(), eventId,eventRepository).run();
	 return true;
	}
	
	
	
	
	
	

}
