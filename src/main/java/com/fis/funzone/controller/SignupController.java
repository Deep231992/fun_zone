package com.fis.funzone.controller;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fis.funzone.model.GameUser;
import com.fis.funzone.model.GameUserRole;
import com.fis.funzone.repository.GameUserRepository;
import com.fis.funzone.repository.GameUserRoleRepository;
import com.fis.funzone.repository.GamesRepository;
import com.fis.funzone.responseModel.LoginResponse;
import com.fis.funzone.responseModel.ResponseSignup;

@RestController
public class SignupController {
	
	@Autowired
	GameUserRepository gameUserRepository;
	
	@Autowired
	GameUserRoleRepository gameUserRoleRepository;
	
	@Autowired
	GamesRepository gameRepository;
	@GetMapping("/")
	public String getTestString() {
		
		return "Hello";
	}

@PostMapping(value="/saveGameUser")	
public ResponseSignup createGameUser(@RequestBody  GameUser user){
	
	System.out.println("user-->"+user.getUserName());
	
	user.setUserCreatedOn(Calendar.getInstance().getTime());
	try {
	if(user!=null) {
			
	gameUserRepository.save(user);
	
	}
	return new ResponseSignup("User Saved Successfully",HttpStatus.CREATED.toString(),1);

	}
	catch (Exception e) {
		System.out.println(e.getMessage());
		return new  ResponseSignup("User not saved",HttpStatus.INTERNAL_SERVER_ERROR.toString(),0);
	}
}
	
@SuppressWarnings("unchecked")
@GetMapping(value="/fetchRoles")
public List<GameUserRole> fetchRoles() {
	
	return ((JpaRepository<GameUserRole, Long>) gameUserRoleRepository).findAll();
}

@PostMapping("/loginGameUser")
public LoginResponse loginUser(@RequestBody GameUser user){
	
GameUser gameUser=gameUserRepository.findByUserNameAndPassword(user.getUserId(), user.getPassword());
LoginResponse response=new LoginResponse("User Verified",gameUser.getUserName(),gameUser.getUserId(),gameUser.getGameUserRole().getRoleName(),gameUser.getGameUserRole().getRoleId(), 1 , HttpStatus.OK.toString(),gameUser.getLastLoginTime());
if(gameUser!=null) {
	gameUser.setLastLoginTime(LocalDateTime.now());
	gameUserRepository.save(gameUser);
	return response;
}
else {
return new LoginResponse("User Not FOund",null,null,null,null,0,HttpStatus.NOT_FOUND.toString(),null);
}
	
}

@GetMapping("/fetchParticipants")
public Set<GameUser> fetchParticipantsList() {
	Set<GameUser> gameUser=gameUserRepository.findParticipants();

	return gameUser;
}

@GetMapping("/fetchGames")
public Set<GameUser> fetchGames() {
	Set<GameUser> gameUser=gameUserRepository.findParticipants();

	return gameUser;
}



}
