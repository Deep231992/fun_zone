package com.fis.funzone.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fis.funzone.model.Event;

@Repository
@Transactional
public interface EventRepository extends JpaRepository<Event, Long> {

	
	public Event findByEventId(long eventId);
	
	@Modifying
	@Query("update Event e set e.Event_status =:status where e.eventId=:eventId")
	public void updateEvent(@Param("status") String status,@Param("eventId") long eventId);
}
