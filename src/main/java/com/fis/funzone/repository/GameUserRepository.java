package com.fis.funzone.repository;


import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fis.funzone.model.GameUser;


@Repository
@org.springframework.transaction.annotation.Transactional
public interface GameUserRepository extends JpaRepository<GameUser, Long> {
	
	@Query("Select u from GameUser u where userId = :userId and password = :password")
	public GameUser findByUserNameAndPassword(@Param("userId")  String userId, @Param("password") String password);
	
	public GameUser findByUserId(String userId);
	
	@Query("Select u from GameUser u left join u.gameUserRole g where g.roleId = u.gameUserRole and g.roleId=2")
	public Set<GameUser> findParticipants();
	

}
