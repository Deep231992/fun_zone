package com.fis.funzone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fis.funzone.model.Games;

@Repository
public interface GamesRepository extends JpaRepository<Games, Long> {

	
	public Games  findByGameName(String gameName);
	
}
