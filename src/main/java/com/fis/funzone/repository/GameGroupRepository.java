package com.fis.funzone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fis.funzone.model.GameGroup;

public interface GameGroupRepository extends JpaRepository<GameGroup, Long> {

	
	
}
