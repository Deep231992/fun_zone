package com.fis.funzone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fis.funzone.model.GameUserRole;


@Repository
@Transactional
public interface GameUserRoleRepository extends JpaRepository<GameUserRole, Long> {
	
	

}
