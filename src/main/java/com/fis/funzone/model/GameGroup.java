package com.fis.funzone.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table
public class GameGroup {
	
	
	
	 /**
	 * 
	 */

	public Long getGroupId() {
		return groupId;
	}
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
//	public Set<GameUser> getGameUser() {
//		return gameUser;
//	}
//	public void setGameUser(Set<GameUser> gameUser) {
//		this.gameUser = gameUser;
//	}
	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 private Long groupId;
	 
	 public Set<UserGroupMapping> getUserGroupMapping() {
		return userGroupMapping;
	}
	public void setUserGroupMapping(Set<UserGroupMapping> userGroupMapping) {
		this.userGroupMapping = userGroupMapping;
	}
	private String groupName;
	 
	@JsonManagedReference
	 @OneToMany(targetEntity=UserGroupMapping.class, mappedBy = "gameGroup", cascade = CascadeType.ALL , orphanRemoval = true , fetch = FetchType.LAZY)	
		private Set<UserGroupMapping> userGroupMapping;	
	 
	@JsonManagedReference
	 @OneToMany(targetEntity=GroupGameMapping.class, mappedBy = "gameGroup", cascade = CascadeType.ALL , orphanRemoval = true , fetch = FetchType.LAZY)	
		private Set<GroupGameMapping> groupGameMapping;	
	 
	 @JsonIgnore
	 @OneToOne(mappedBy = "gameGroup",fetch = FetchType.LAZY)
	 
	 private Event event;

	public Set<GroupGameMapping> getGroupGameMapping() {
		return groupGameMapping;
	}
	public void setGroupGameMapping(Set<GroupGameMapping> groupGameMapping) {
		this.groupGameMapping = groupGameMapping;
	}
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	
	 
	
	

}
