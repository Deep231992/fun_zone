package com.fis.funzone.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Table
@Entity
public class GroupGameMapping {

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Games getGames() {
		return games;
	}

	public void setGames(Games games) {
		this.games = games;
	}

	public GameGroup getGameGroup() {
		return gameGroup;
	}

	public void setGameGroup(GameGroup gameGroup) {
		this.gameGroup = gameGroup;
	}

	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 private Long id;
	
	@ManyToOne()
	@JoinColumn(name = "gamesId_fk")
	@JsonBackReference
	private Games games;
	
	@ManyToOne()
	@JoinColumn(name = "groupId_fk")
	@JsonBackReference
	private GameGroup gameGroup;
}
