package com.fis.funzone.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table
public class GameUserRole  {
	
	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="RoleId",nullable=false)
	private Long roleId;
private String roleName;

@JsonIgnore
@OneToMany(targetEntity=GameUser.class, mappedBy = "gameUserRole", cascade = CascadeType.ALL , orphanRemoval = true)
private Set<GameUser> gameUser;


public Set<GameUser> getGameUser() {
	return gameUser;
}
public void setGameUser(Set<GameUser> gameUser) {
	this.gameUser = gameUser;
}
public Long getRoleId() {
	return roleId;
}
public void setRoleId(Long roleId) {
	this.roleId = roleId;
}
public String getRoleName() {
	return roleName;
}
public void setRoleName(String roleName) {
	this.roleName = roleName;
}


	

}
