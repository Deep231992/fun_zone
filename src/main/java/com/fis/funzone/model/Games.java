package com.fis.funzone.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Games {

	
	private String gameName;


	@OneToMany(targetEntity=GroupGameMapping.class, mappedBy = "games", cascade = CascadeType.ALL , orphanRemoval = true)	
	private Set<GroupGameMapping> groupGameMapping;	
	
	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 private Long id;
}
