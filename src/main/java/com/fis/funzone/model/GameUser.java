package com.fis.funzone.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table
public class GameUser  {

	
	 /**
	 * 
	 */
	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 private Long id;
	private String emailId;
	 public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	
	@Column(length=128,unique=true)
	private String userId;
	 private String userName;
	
	 private String password;
	 private String designation;
	 
	 
	 
	 private LocalDateTime LastLoginTime;
	 
	
	public LocalDateTime getLastLoginTime() {
		return LastLoginTime;
	}
	public void setLastLoginTime(LocalDateTime lastLoginTime) {
		LastLoginTime = lastLoginTime;
	}

	@Temporal(TemporalType.DATE)
	 private Date userCreatedOn;
	 
        
		public Date getUserCreatedOn() {
		return userCreatedOn;
	}
	public void setUserCreatedOn(Date userCreatedOn) {
		this.userCreatedOn = userCreatedOn;
	}
	

		@ManyToOne()
		@JoinColumn(name = "gameUserRoleId_FK")
	 private GameUserRole gameUserRole;
		
	@OneToMany(targetEntity=UserGroupMapping.class, mappedBy = "gameUser", cascade = CascadeType.ALL , orphanRemoval = true)	
	private Set<UserGroupMapping> userGroupMapping;	
	 
	 public Set<UserGroupMapping> getUserGroupMapping() {
		return userGroupMapping;
	}
	public void setUserGroupMapping(Set<UserGroupMapping> userGroupMapping) {
		this.userGroupMapping = userGroupMapping;
	}
	public GameUserRole getGameUserRole() {
		return gameUserRole;
	}
	public void setGameUserRole(GameUserRole gameUserRole) {
		this.gameUserRole = gameUserRole;
	}
	


	
	 
}
