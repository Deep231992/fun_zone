package com.fis.funzone.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table
public class UserGroupMapping {

	
	
	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 private Long id;	
	
	@ManyToOne()
	@JoinColumn(name = "groupId_fk")
	@JsonBackReference
	private GameGroup gameGroup;
	
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public GameGroup getGameGroup() {
		return gameGroup;
	}


	public void setGameGroup(GameGroup gameGroup) {
		this.gameGroup = gameGroup;
	}


	public GameUser getGameUser() {
		return gameUser;
	}


	public void setGameUser(GameUser gameUser) {
		this.gameUser = gameUser;
	}


	@ManyToOne()
	@JoinColumn(name="gameUser_fk")
	@JsonBackReference
	private GameUser gameUser;
	
	
	
}
