package com.fis.funzone.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Table
@Entity
public class Event {
	
	
	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 private Long id;
	
	private String eventName;
	
//	Event CreatedBy   Event_Created_Time Group_Id Event_Duration  Event_status
	private String CreatedBy;
	private int Event_Duration;
	
	@Column(length = 15,unique = true)
	private long eventId;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public int getEvent_Duration() {
		return Event_Duration;
	}

	public void setEvent_Duration(int event_Duration) {
		Event_Duration = event_Duration;
	}

	public long getEventId() {
		return eventId;
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public String getEvent_status() {
		return Event_status;
	}

	public void setEvent_status(String event_status) {
		Event_status = event_status;
	}

	public GameGroup getGameGroup() {
		return gameGroup;
	}

	public void setGameGroup(GameGroup gameGroup) {
		this.gameGroup = gameGroup;
	}

	@Column(length = 1)
	private String Event_status;
	
	@OneToOne()
	@JoinColumn(name="gameGroupId_fk")
	private GameGroup gameGroup;
	

}
